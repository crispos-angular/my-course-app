import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product, ProductResponse } from 'src/app/models/Product';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

//to use the service anywhere on the project
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ){}

  getProducts(): Observable<ProductResponse>{
    //api call to return products information
    return this.http.get<ProductResponse>(this.apiUrl + '/products');
  }


}
