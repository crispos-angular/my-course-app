import { Product, ProductResponse } from 'src/app/models/Product';
import { ProductService } from './../../services/product.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgOptimizedImage } from '@angular/common'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy{

  subscription: Subscription | undefined;

  products: Product[] | undefined = [];

  constructor(
    private productService: ProductService
  ){ }

  ngOnInit(){
    this.subscription = this.productService.getProducts().subscribe((response: ProductResponse) => {
      console.log('Request successful')
      console.log(response);
      this.products = response.products;
    },error => {
      console.error(error);
    },() => {
      console.log("I finished");
    })
  }

  ngOnDestroy(){
    this.subscription?.unsubscribe();
  }


}
