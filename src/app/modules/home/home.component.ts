import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Observable, Subscription, of } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{
  title = 'my-course-app';

  headerTitle = 'A header created on app.component.ts';
  footerTitle = 'A footer created on app.component.ts';
  isHeaderVisible = true;

  name: string | undefined;
  lastName: string | undefined;


  constructor(
    private FormBuilder: FormBuilder,
    private router: Router
  ){

  }

  formGroup: FormGroup | undefined;

  //observable
  observable$: Observable<number> = of(1,2,3,4,5)

  //to manage the subscriptions
  subscription: Subscription | undefined;
  formSubscription: Subscription | undefined;

  ngOnInit(): void {
    this.formGroup = this.FormBuilder.group({
      name: [undefined, [Validators.required, Validators.minLength(3)]],
      lastName: undefined
    })
  }

  onFormSubmit(): void{
    console.log(this.formGroup?.value);
  }

  goToProfileAndLogCabbage(): void {
    console.log('Cabbage');
    this.router.navigate(['/profile'])
  }

}
