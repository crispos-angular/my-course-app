import { Component, Input, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{

  //directives
  @Input() title: string | undefined = 'Header title';
  //create variables here and initialize them if you want to have a predefine value, but that can be override from other components
  array = [1,2,3,4,5]

  capitalText  = 'CAPITAL TEXT';

  birthDay = new Date(1994,6,23);

  constructor() {}

  ngOnInit(): void {
    //variables should be initialize inside here if they won't be override from other components, at least not while inside the ngOnInit
    console.log(this.title)
  }


}
