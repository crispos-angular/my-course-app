import { VersionService } from './../../../../services/version.service';
import { Component, OnInit, Input, Version } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit{

  @Input() title: string | undefined;

  version: string | undefined;

  constructor(
    private versionService: VersionService
  ){}

  ngOnInit(): void {
    this.version = this.versionService.getVersion();
  }

}
