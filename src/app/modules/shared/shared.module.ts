import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BirthdayPipe } from './pipes/birthday.pipe';
import { FooterComponent } from './layout/footer/footer.component';
import { HeaderComponent } from './layout/header/header.component';

const components = [
  HeaderComponent,
  FooterComponent,
]

const pipes = [
  BirthdayPipe,
]

const modules = [
  CommonModule,
]

@NgModule({
  imports: [...modules],
  declarations: [
    ...components,
    ...pipes,
  ],
  exports: [
    ...components
  ]
})
export class SharedModule { }
